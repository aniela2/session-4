package ro.aniela.object;

public class Exercise16 {
    public static void main(String[] args) {
        Person p = new Person();
        p.age = 18;
        p.name = "Elena";
        p.surname = "Soare";
        System.out.println(p.name + " " + p.surname + " has " + p.age);

        Person c = new Person();
        c.age=22;
        c.name= "Ana";
        c.surname="Crin";
        System.out.println(c.name + " " + c.surname + " has " + c.age);
    }

}
