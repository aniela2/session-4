package ro.aniela.object;

public class Exercise22 {
    public static void main(String[] args) {
        double[] monthSalaries = {43.0, 55.9};
        Address adress = new Address("roma", "stefan cel mare", 2);
        Employee m = new Employee("N", "Alin", 21, monthSalaries, adress);
        System.out.println(m);
        System.out.println(averageSalary(m.getMonthSalaries()));
        System.out.println(increasedSalary(m.getMonthSalaries(), 1.20));
    }

    private static double sumOfSalaries(double[] salary) {
        double sum = 0d;
        for (double i : salary) {
            sum = i + sum;
        }
        return sum;
    }

    private static double averageSalary(double[] salary) {
        return sumOfSalaries(salary) / salary.length;
    }

    private static double increasedSalary(double[] salary, double percent) {
        return sumOfSalaries(salary) * percent;
    }
}