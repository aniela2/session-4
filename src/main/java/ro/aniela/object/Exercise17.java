package ro.aniela.object;

public class Exercise17 {

    public static void main (String[] args){
        Person p= new Person();
        p.surname="Ion";
        p.name = "cri";
        p.age=21;
        Person c= new Person();
        c.surname="cr";
        c.name="vali";
        c.age= 29;

        Person[] persons= {p,c};

        printPerson(persons);

    }

    private static void printPerson(Person[] persons){
        for (Person p :persons){
            if(p.age>=18){
                System.out.println(p.name.toUpperCase() + p.surname.toLowerCase());
            }
        }
    }
}
