package ro.aniela.object;

public class Book {
    private String author;
    private int releaseYear;
    private double price;

    public Book(){}

    public void setAuthor(String author){
        this.author=author;
    }
    public void setReleaseYear(int releaseYear){
        this.releaseYear=releaseYear;

    }
    public void setPrice(double price){
        this.price= price;

    }

    public String getAuthor(){
        return author;
    }

    public int getReleaseYear(){
        return releaseYear;
    }

    public double getPrice(){
        return price;

    }



}
