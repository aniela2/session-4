package ro.aniela.object;

public class Car {
    private String color;
    private String type;
    private double maxSpeed;

    public Car() {
    }

    public Car(String color, String type, double maxSpeed) {
        this.color = color;
        this.type = type;
        this.maxSpeed = maxSpeed;
    }

    public void repaint (String color){
        this.color = color;
    }
    public static void drive (){
        System.out.println("drive");
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getColor() {
        return color;
    }

    public String getType() {
        return type;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public String toString() {
        return "Car : color" + color + "type" + type + "maxspeed" + maxSpeed;
    }
}
