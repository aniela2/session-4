package ro.aniela.object;

public class Exercise23 {

    public static void main(String[] args) {

        Book p = new Book();
        p.setPrice(23.5);
        p.setAuthor("LE");
        p.setReleaseYear(1932);
        System.out.println(p.getAuthor() + " " + p.getPrice() + " " + p.getReleaseYear());

        Book book = new Book();
        book.setReleaseYear(1990);
        book.setAuthor("ia");
        book.setPrice(21.0);
        System.out.println(book.getAuthor() + book.getPrice() + book.getReleaseYear());

    }


}
