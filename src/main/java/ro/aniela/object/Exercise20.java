package ro.aniela.object;

public class Exercise20 {

    public static void main(String[] args) {

        Building p = new Building(2, "Caragiale", 23, "Roma");
        System.out.println(p.getCity() + p.getStreet() + p.getStreetNumber() + p.getNoOfFloors());
        p.setNoOfFloors(3);
        System.out.println(p.getCity() + p.getStreet() + p.getStreetNumber() + p.getNoOfFloors());

        Building c=new Building(4,"str", 25, "Flo");
        System.out.println(c.getCity() + c.getStreet() + c.getNoOfFloors()+ c.getStreetNumber());

        Building stGt= new Building();
        System.out.println(stGt);
        stGt.setNoOfFloors(5);
        stGt.setCity("num");
        stGt.setStreet("str");
        stGt.setStreetNumber(1);
        System.out.println(stGt.getCity() + stGt.getStreet() + stGt.getStreetNumber()+stGt.getNoOfFloors());

        System.out.println(c.toString());


    }
}
