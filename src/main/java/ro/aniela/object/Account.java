package ro.aniela.object;

public class Account {
    private Person person;
    private double balance;

    public Account(Person person, double balance) {
        this.person = person;
        this.balance = balance;
    }

    public void credit(double amount) {
        double sum = balance + amount;
        this.balance = sum;
        //this.balance+=amount;
    }

    public void debit(double amount) {
        this.balance -= amount;
    }

    public Person getPerson() {
        return person;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "Account: person " + person + ", balance " + balance;
    }

}
