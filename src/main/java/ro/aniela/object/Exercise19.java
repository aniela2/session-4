package ro.aniela.object;

public class Exercise19 {
   // Define a class called Account, which models a bank account of a customer, with the following:
   // field/s (attribute/s): Person (used the previous created class) and balance
//    method/s (behavior/s):
//    credit - add the given amount to the balance
//    debit - subtract the given amount to the balance.

    public static void main(String[]args){
        Person person= new Person();
        person.name="OAna";
        person.surname="ana";
        person.age=18;
        Account c =new Account(person,100.0);
        c.credit(50.0);
        System.out.println(c);
        c.debit(30.0);
        System.out.println(c);

    }
}
