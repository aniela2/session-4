package ro.aniela.object;

import java.util.Arrays;

public class Employee {
    private String name;
    private String surName;
    private int age;
    private double[] monthSalaries;
    private Address address;

    public Employee(String name, String surName, int age, double[] monthSalaries, Address address) {
        this.name = name;
        this.surName = surName;
        this.age = age;
        this.monthSalaries = monthSalaries;
        this.address = address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }
    public void setAge(int age){
        this.age=age;
    }
    public void setMonthSalaries(double[] monthSalaries){
        this.monthSalaries=monthSalaries;
    }
    public void setAddress(String adress){
        this.address=address;
    }

    public String getName(){
        return name;
    }
    public String getSurName(){
        return surName;
    }
    public int getAge(){
        return age;
    }
    public double[] getMonthSalaries(){
        return monthSalaries;
    }
    public Address getAddress(){
        return address;
    }


    public String toString (){
        return "Employee : name "+ name + "surname" + surName +  "age" + age + "monthsalaries" + Arrays.toString(monthSalaries) + "adress "+ address;
    }

}

