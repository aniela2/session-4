package ro.aniela.object;

public class MathematicalOp {
    private double number1;
    private double number2;

    public MathematicalOp(double number1, double number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    public double sum() {
        return number1 + number2;
    }

    public double substract() {
        return number1 - number2;
    }

    public double product() {
        return number1 * number2;
    }

    public double devide() {
        return number1 / number2;
    }

    public double modulo() {
        return number1 % number2;
    }

    public void setNumber1(double number1) {
        this.number1 = number1;
    }

    public void setNumber2(double number2) {
        this.number2 = number2;
    }

    public double getNumber1() {
        return number1;
    }

    public double getNumber2() {
        return number2;
    }

}
