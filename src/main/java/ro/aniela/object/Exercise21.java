package ro.aniela.object;

public class Exercise21 {
   public  static void main(String[]args){

       Car n= new Car();
       n.setColor("verde");
       n.setType("wls");
       n.setMaxSpeed(1);
       n.repaint("alb");
       System.out.println(n);
       Car.drive();
       Car z = new Car("verde","ww",200);
       z.repaint("gri");
       Car.drive();
   }
}
