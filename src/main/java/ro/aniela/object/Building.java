package ro.aniela.object;

public class Building {

    private int noOfFloors;
    private String street;
    private int streetNumber;
    private String city;

    public Building() {
    }


    public Building(int noOfFloors, String street, int streetNumber, String city) {
        this.noOfFloors = noOfFloors;
        this.street = street;
        this.streetNumber = streetNumber;
        this.city = city;
    }

    public void setNoOfFloors(int noOfFloors) {
        this.noOfFloors = noOfFloors;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public int getNoOfFloors() {
        return noOfFloors;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public String toString() {
        return "Building : noOfFloors " + noOfFloors + ", street " + street + ", streetNumber " + streetNumber + ", city " + city;
    }

}
