package ro.aniela.string;

public class Exercise9 {
    // Given two Strings, write a program that determines if the first String contains the second String or vice-versa.


    public static void main(String[] args) {
System.out.print(containsS("ana are mere", "mere"));

    }

    private static boolean containsS(String one, String two) {
        boolean a = one.contains(two);
        boolean b = two.contains(one);
        return a || b ;
    }
}
