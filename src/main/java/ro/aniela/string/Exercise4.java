package ro.aniela.string;

public class Exercise4 {
    public static void main(String[] args) {
        String[] y = {"Ana are mere","1 2 3"};
        System.out.println(replacesS(y));

    }

    //Given an array of String, write a program to print into the console each  String by replacing all spaces with star character (*).
    private static String replacesS(String[] args) {
        String output = "";
        for (String s : args) {
            // System.out.println(s.replaceAll(" ","*"));
            output = output + s.replaceAll(" ", "*") + " ";
        }
        return output;
    }
}
