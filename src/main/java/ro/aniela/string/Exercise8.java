package ro.aniela.string;

public class Exercise8 {
    public static void main(String[] args) {
        String[] z = {"antena", "unu", "are", " ceva "};
        characterGiven(z, "a");

    }

    //Given an array of String, write a program that prints into the console only the
    // String elements that start with a given character.
    private static void characterGiven(String[] args, String y) {

        for (String i : args) {

            if (i.startsWith(y)) {
                System.out.println(i);
            }
        }
    }
}
