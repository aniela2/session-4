package ro.aniela.string;

public class Exercise10 {
    public static void main(String[] args) {
System.out.println(duplicateCharacter("banana"));
    }

    // Given a String, write a program to determine which is the first duplicate character;

    private static String duplicateCharacter(String args) {
        String[] letters = args.split("");
        String lett = "";
        for (String l : letters) {
            if (lett.contains(l)) {
                return l;
            }
            lett = lett + l;

        }
        return "";
    }
}

