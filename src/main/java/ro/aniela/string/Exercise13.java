package ro.aniela.string;

public class Exercise13 {
    public static void main(String[] args) {
        swapStrings("adrian", "ionescu");

    }

    ////Write a Java program to swap two Strings and print them into the console.
    private static void swapStrings(String one, String two) {
        String args = one;
        one = two;
        two = args;
        System.out.println(one + " " + two);
    }

    ////Write a Java program to swap two Strings and print them into the console.
}

