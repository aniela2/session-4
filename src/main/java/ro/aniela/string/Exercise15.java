package ro.aniela.string;

public class Exercise15 {

    public static void main(String[] args) {
        String[] y = {"1", "1", "2"};
        System.out.print((sumOfStrings(y)));

    }
    //Given an array of Strings (each String is a number), write a program to calculate the sum.

    private static int sumOfStrings(String[] args) {
        int sum = 0;
        for (String s : args) {
            sum = sum + Integer.parseInt(s);

        }
        return sum;


    }
}
