package ro.aniela.string;

public class Exercise6 {
    public static void main(String[] args) {
        separateByPipe(new String[]{"one", "lasr", "te", "1"});
        System.out.println();
        pipeSeparated(new String[]{"1", "2", "3"});
    }

    private static void separateByPipe(String[] args) {

        for (String each : args) {
            System.out.print(each + '|');
        }
    }

    private static void pipeSeparated(String[] ards) {
        String s = "";
        for (String i : ards) {
            s = s + i + '|';
        }
        s = s.substring(0, s.length() - 1);
        System.out.println(s);


    }
}
