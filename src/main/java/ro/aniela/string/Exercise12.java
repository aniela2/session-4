package ro.aniela.string;

public class Exercise12 {
    public static void main(String[] args) {
        System.out.println(repeatString("ererer", "re"));
        System.out.print(repeatR("eererer", "er"));
    }

    //Given a String, write a program to determine how many times a given String is present inside that given String.
    private static int repeatString(String word, String founded) {
        String[] name = word.split(founded);
        return name.length - 1;
    }

    private static int repeatR(String word, String founded) {
        int index = word.lastIndexOf(founded);
        if (index == -1) {
            return 0;

        } else {
            int sum =0;
            while (index != -1) {
                word = word.substring(0, index);
                index = word.lastIndexOf(founded);
                sum++;
            }
            return sum;
        }


    }


}

