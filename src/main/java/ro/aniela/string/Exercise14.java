package ro.aniela.string;

import java.util.Arrays;

public class Exercise14 {
    public static void main(String[] args) {
        char[] y = {'h', 'e', 'l','l','o'};
        System.out.print(convertToString(y));


    }

    //Given an array of chars, write a program to convert the array to a String.

    private static String convertToString(char[] chars) {
        String sum= "";
        for (char character : chars) {
            sum=sum + character;
        }
        return sum;

    }
}
