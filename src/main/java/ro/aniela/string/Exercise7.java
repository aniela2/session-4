package ro.aniela.string;

public class Exercise7 {
    public static void main(String[] args) {
        System.out.println(countCharacters(new String[]{"one", "jhs", " "}));

    }

    private static int countCharacters(String[] args) {

        int sum = 0;
        for (String element : args) {
            sum = sum + element.length();
        }
        return sum;
    }
}
